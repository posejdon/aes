from aes import AES


def testAES(mode, key, IV, plain, cipher):
    _key = key.decode('hex')
    _IV = IV.decode('hex')
    _plain = plain.decode('hex')
    _cipher = cipher.decode('hex')

    aes = AES(_key, _IV, mode)

    encrypted = aes.encrypt(_plain)
    decrypted = aes.decrypt(_cipher)

    assert encrypted == bytearray(_cipher)
    assert decrypted == bytearray(_plain)

    print 'Passed for\nMode:{}\nKey{}\nIV:{}\nPlaintext:{}\nCiphertext:{}\n'.format(
        mode,
        key,
        IV,
        ''.join('{:02x}'.format(x) for x in decrypted),
        ''.join('{:02x}'.format(x) for x in encrypted),
    )

if __name__ == '__main__':

    testAES(
        'CBC',
        '00000000000000000000000000000000',
        '00000000000000000000000000000000',
        'f34481ec3cc627bacd5dc3fb08f273e6',
        '0336763e966d92595a567cc9ce537f5e',
    )

    testAES(
        'CBC',
        '000000000000000000000000000000000000000000000000',
        '00000000000000000000000000000000',
        '1b077a6af4b7f98229de786d7516b639',
        '275cfc0413d8ccb70513c3859b1d0f72',
    )

    testAES(
        'CBC',
        '0000000000000000000000000000000000000000000000000000000000000000',
        '00000000000000000000000000000000',
        '014730f80ac625fe84f026c60bfd547d',
        '5c9d844ed46f9885085e5d6a4f94c7d7',
    )

    testAES(
        'CFB',
        '00000000000000000000000000000000',
        'f34481ec3cc627bacd5dc3fb08f273e6',
        '00000000000000000000000000000000',
        '0336763e966d92595a567cc9ce537f5e',
    )

    testAES(
        'CFB',
        '000000000000000000000000000000000000000000000000',
        '1b077a6af4b7f98229de786d7516b639',
        '00000000000000000000000000000000',
        '275cfc0413d8ccb70513c3859b1d0f72',
    )

    testAES(
        'CFB',
        '0000000000000000000000000000000000000000000000000000000000000000',
        '014730f80ac625fe84f026c60bfd547d',
        '00000000000000000000000000000000',
        '5c9d844ed46f9885085e5d6a4f94c7d7',
    )

    testAES(
        'ECB',
        '00000000000000000000000000000000',
        '',
        'f34481ec3cc627bacd5dc3fb08f273e6',
        '0336763e966d92595a567cc9ce537f5e',
    )

    testAES(
        'ECB',
        '000000000000000000000000000000000000000000000000',
        '',
        '1b077a6af4b7f98229de786d7516b639',
        '275cfc0413d8ccb70513c3859b1d0f72',
    )

    testAES(
        'OFB',
        '00000000000000000000000000000000',
        'f34481ec3cc627bacd5dc3fb08f273e6',
        '00000000000000000000000000000000',
        '0336763e966d92595a567cc9ce537f5e',
    )

    testAES(
        'OFB',
        '000000000000000000000000000000000000000000000000',
        '1b077a6af4b7f98229de786d7516b639',
        '00000000000000000000000000000000',
        '275cfc0413d8ccb70513c3859b1d0f72',
    )

    testAES(
        'OFB',
        '0000000000000000000000000000000000000000000000000000000000000000',
        '014730f80ac625fe84f026c60bfd547d',
        '00000000000000000000000000000000',
        '5c9d844ed46f9885085e5d6a4f94c7d7',
    )

    testAES(
        'CTR',
        '2b7e151628aed2a6abf7158809cf4f3c',
        'f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff',
        '6bc1bee22e409f96e93d7e117393172a',
        '874d6191b620e3261bef6864990db6ce',
    )

    testAES(
        'CTR',
        '8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b',
        'f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff',
        '6bc1bee22e409f96e93d7e117393172a',
        '1abc932417521ca24f2b0459fe7e6e0b',
    )

    testAES(
        'CTR',
        '603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4',
        'f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff',
        '6bc1bee22e409f96e93d7e117393172a',
        '601ec313775789a5b7a7f504bbf3d228',
    )




